// Note that Xcode gets this line wrong.  You need to change "Cordova.h" to "CDV.h" as shown below.
#import <Cordova/CDV.h>

@interface RandomCordovaPlugin : CDVPlugin <>

// Cordova command method
-(void) randomString:(CDVInvokedUrlCommand*)command;

// Create and override some properties and methods (these will be explained later)
-(void) genRandStringLength:(int) len;
@property (strong, nonatomic) CDVInvokedUrlCommand *latestCommand;


@end


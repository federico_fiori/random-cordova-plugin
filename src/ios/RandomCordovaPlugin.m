#import "RandomCordovaPlugin"
#import "AppDelegate.h"
@implementation RandomCordovaPlugin

// Cordova command method
-(void) randomString:(CDVInvokedUrlCommand *)command {

	// Save the CDVInvokedUrlCommand as a property.  We will need it later.
	self.latestCommand = command;

	// Make the overlay view controller.
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController *rootViewController = delegate.window.rootViewController;
}


-(NSString *) genRandStringLength: (int) len {

    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];

    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }

    return randomString;
}

@end